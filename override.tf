resource "proxmox_vm_qemu" "k3s-server" {
    # [VM]
    qemu_os = "l26"
    clone_wait = 0
    additional_wait = 0
    # Activate QEMU agent for this VM
    agent = 1
    
    # [STORAGE]
    os_type = "cloud-init"
    boot = "order=scsi0"
    full_clone  = true

    # [DISPLAY]
    vga {
      memory = 0
      type   = "serial0"
    }

    # [SSH]
    ciuser = "ubuntu"
    define_connection_info = true
    ssh_user = "ubuntu"
    connection {
      type        = "ssh"
      host        = "${self.ssh_host}"
      user        = "${self.ciuser}"
      private_key = file("~/.ssh/id_rsa")
      port        = 22
    }    
}